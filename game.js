const startEasy = document.querySelector('.startGameEasy');
const startMedium = document.querySelector('.startGameMedium');
const startHard = document.querySelector('.startGameHard');

const startContent = document.querySelector('#startScreen');

const guessingGame = document.querySelector('#guessingGameEasy');
const guessingGameMedium = document.querySelector('#guessingGameMedium');
const guessingGameHard = document.querySelector('#guessingGameHard');

const pageOverflow = document.querySelector('body');

const menuEasy = document.querySelector('#menuEasy');
const menuMedium = document.querySelector('#menuMedium');
const menuHard = document.querySelector('#menuHard');


//This algoritm will pick a random number from 1-100 for the user to guess
var randomNumber;

function showStartScreen(){
  startContent.classList.remove('hideStartScreen');
}

function hideStartScreen(){
  startContent.classList.add('hideStartScreen')
}

function easyDiff(){ //
  hideStartScreen(); //
  guessingGame.style.display = 'block'; //
  pageOverflow.style.overflowY = 'visible'; 
  randomNumber = Math.floor(Math.random() * 100) + 1;
}

function mainMenuEasy(){
  showStartScreen();
  guessingGame.style.display = 'none';
  pageOverflow.style.overflowY = 'hidden';
  resetGameEasy();
}

function mediumDiff(){ //
  hideStartScreen(); //
  guessingGameMedium.style.display = 'block'; //
  pageOverflow.style.overflowY = 'visible';
  randomNumber = Math.floor(Math.random() * 500) + 1;
}

function mainMenuMedium(){
  showStartScreen();
  guessingGameMedium.style.display = 'none';
  pageOverflow.style.overflowY = 'hidden';
  resetGameMedium();
}

function hardDiff(){ //
  hideStartScreen(); //
  guessingGameHard.style.display = 'block'; //
  pageOverflow.style.overflowY = 'visible';
  randomNumber = Math.floor(Math.random() * 1000) + 1;
}

function mainMenuHard(){
  showStartScreen();
  guessingGameHard.style.display = 'none';
  pageOverflow.style.overflowY = 'hidden';
  resetGameHard();
}



startEasy.addEventListener('click', easyDiff);
menuEasy.addEventListener('click', mainMenuEasy)

startMedium.addEventListener('click', mediumDiff);
menuMedium.addEventListener('click', mainMenuMedium)

startHard.addEventListener('click', hardDiff);
menuHard.addEventListener('click', mainMenuHard)




//Set a constant to target the first 2 <p> tags
const guessesEasyHigh = document.querySelector('.guessesEasyHigh');
const guessesEasyLow = document.querySelector('.guessesEasyLow');

const guessesMediumHigh = document.querySelector('.guessesMediumHigh');
const guessesMediumLow = document.querySelector('.guessesMediumLow');

const guessesHardHigh = document.querySelector('.guessesHardHigh');
const guessesHardLow = document.querySelector('.guessesHardLow');

//Set a constant to target the second <p> tag
const lastResultEasy = document.querySelector('.lastResultEasy');
const lastResultMedium = document.querySelector('.lastResultMedium');
const lastResultHard = document.querySelector('.lastResultHard');

//Set a constant to target the third <p> tag
const lowOrHiEasy = document.querySelector('.lowOrHiEasy');
const lowOrHiMedium = document.querySelector('.lowOrHiMedium');
const lowOrHiHard = document.querySelector('.lowOrHiHard');

//Set a constant to target the Submit guess button
const guessSubmitEasy = document.querySelector('.guessSubmitEasy');
const guessSubmitMedium = document.querySelector('.guessSubmitMedium');
const guessSubmitHard = document.querySelector('.guessSubmitHard');

//Set a constant to target the number entered in the input
const guessFieldEasy = document.querySelector('.guessFieldEasy');
const guessFieldMedium = document.querySelector('.guessFieldMedium');
const guessFieldHard = document.querySelector('.guessFieldHard');

//Set the guess count to start at 1
let guessCount = 1;

//Name a variable with no value for the reset button
let resetButton;
let resetButtonMedium;
let resetButtonHard;

const gamePage = document.querySelector('.wrapper');

const gameReset = document.querySelector('.reset');
const gameResetMedium = document.querySelector('.resetMedium');
const gameResetHard = document.querySelector('.resetHard');

/*
This function checks the guess(es) entered
First the variable userGuess is set to take the value of the input and convert it to a number
Second an if statement takes the variable guessCount and checks if guessCount is equal to 1. 
True renders the text 'Previous guesses: '
False displays each guess in order with a space
Third the if else statements first parameter checks if the guess is correct.
Correct displays some positive text with a background color of green and display the reset button.
Incorrect moves on to check the else if to see if the user reached their guess limit. If so display *Sad Violin Noises* and reset button.
The final execution in this if statement tells the user their guess is wrong and runs a new else if.
This new else if checks if the users guess was greater than or less than the random number and returns a string accordingly.
Next the guessCount updates and adds 1.
The guess input is cleared for a new guess.
Last focus allows the user to enter a new guess without clicking the input box.
*/
function checkGuessEasy() { //Changed checkGuess to CheckGuessEasy
  let userGuess = Number(guessFieldEasy.value); //changed guessField to guessFieldEasy
  if (guessCount === 1) {
    guessesEasyHigh.textContent = 'Too High: ';
    guessesEasyLow.textContent = 'Too Low: '
  }
 
  if (userGuess === randomNumber) {
    lastResultEasy.textContent = 'Congratulations! You got it right!';
    lastResultEasy.style.backgroundColor = 'limeGreen';
    lowOrHiEasy.textContent = '';
    setGameOverEasy();
  } else if (guessCount === 10) {
    lastResultEasy.textContent = '*Sad Violin Noises*';
    setGameOverEasy();
  } else { 
    lastResultEasy.textContent = 'Wrong!';
    lastResultEasy.style.backgroundColor = 'orangeRed';
    if(userGuess < randomNumber) {
      guessesEasyLow.textContent += userGuess + ' ';
      lowOrHiEasy.textContent = 'Last guess was too low!';
    } else if(userGuess > randomNumber) {
      guessesEasyHigh.textContent += userGuess + ' ';
      lowOrHiEasy.textContent = 'Last guess was too high!';
    }
  }
 
  guessCount++;
  guessFieldEasy.value = '';
  guessFieldEasy.focus();
}

//Tell the browser to run the checkGuess() function each time the user submits a guess.
guessSubmitEasy.addEventListener('click', checkGuessEasy);

/*
This function prompts the user to start a new game.
First disable the guess input box so the user cannot input any new guesses.
Disable the Submit guess button.
Create the reset button.
Display 'Start new game' inside button.
Display reset button in the div with the class of reset.
On click run function resetGame.
*/
function setGameOverEasy() {
  guessFieldEasy.disabled = true;
  guessSubmitEasy.disabled = true;
  resetButton = document.createElement('button');
  resetButton.textContent = 'Start new game';
  gameReset.appendChild(resetButton);
  resetButton.addEventListener('click', resetGameEasy);
}

/*
This function will reset the game
First set the guess count back to 1.
Set a constant that selects the <p> tags in the game <div>.
Run a for loop that resets the paragraph text to an empty string until all the paragraphs are cleared.
Access the reset button from the setGameOver function and remove it.
Re-enable the guess input box.
Re-enable the Submit guess button.
Set the guess input box field to an empty string.
Set focus onto the guess input box.
Previous guess background set to blank white.
Assign randomNumber a new number from 1-100.
*/
function resetGameEasy() {
  guessCount = 1;

  const resetParas = document.querySelectorAll('.gameEasy p');
  for (let i = 0 ; i < resetParas.length ; i++) {
    resetParas[i].textContent = '';
  }

  resetButton.parentNode.removeChild(resetButton);

  guessFieldEasy.disabled = false;
  guessSubmitEasy.disabled = false;
  guessFieldEasy.value = '';
  guessFieldEasy.focus();

  lastResultEasy.style.backgroundColor = 'white';

  randomNumber = Math.floor(Math.random() * 100) + 1;
}



function checkGuessMedium() { //Changed checkGuess to CheckGuessEasy
  let userGuess = Number(guessFieldMedium.value); //changed guessField to guessFieldEasy
  if (guessCount === 1) {
    guessesMediumHigh.textContent = 'Too High: ';
    guessesMediumLow.textContent = 'Too Low: ';
  }
 
  if (userGuess === randomNumber) {
    lastResultMedium.textContent = 'Congratulations! You got it right!';
    lastResultMedium.style.backgroundColor = 'limeGreen';
    lowOrHiMedium.textContent = '';
    setGameOverMedium();
  } else if (guessCount === 15) {
    lastResultMedium.textContent = '*Sad Violin Noises*';
    setGameOverMedium();
  } else { 
    lastResultMedium.textContent = 'Wrong!';
    lastResultMedium.style.backgroundColor = 'orangeRed';
    if(userGuess < randomNumber) {
      guessesMediumLow.textContent += userGuess + ' ';
      lowOrHiMedium.textContent = 'Last guess was too low!';
    } else if(userGuess > randomNumber) {
      guessesMediumHigh.textContent += userGuess + ' ';
      lowOrHiMedium.textContent = 'Last guess was too high!';
    }
  }
 
  guessCount++;
  guessFieldMedium.value = '';
  guessFieldMedium.focus();
}

//Tell the browser to run the checkGuess() function each time the user submits a guess.
guessSubmitMedium.addEventListener('click', checkGuessMedium);

function setGameOverMedium() {
  guessFieldMedium.disabled = true;
  guessSubmitMedium.disabled = true;
  resetButtonMedium = document.createElement('button');
  resetButtonMedium.textContent = 'Start new game';
  gameResetMedium.appendChild(resetButtonMedium);
  resetButtonMedium.addEventListener('click', resetGameMedium);
}

function resetGameMedium() {
  guessCount = 1;

  const resetParas = document.querySelectorAll('.gameMedium p');
  for (let i = 0 ; i < resetParas.length ; i++) {
    resetParas[i].textContent = '';
  }

  resetButtonMedium.parentNode.removeChild(resetButtonMedium);

  guessFieldMedium.disabled = false;
  guessSubmitMedium.disabled = false;
  guessFieldMedium.value = '';
  guessFieldMedium.focus();

  lastResultMedium.style.backgroundColor = 'white';

  randomNumber = Math.floor(Math.random() * 500) + 1;
}


function checkGuessHard() { //Changed checkGuess to CheckGuessEasy
  let userGuess = Number(guessFieldHard.value); //changed guessField to guessFieldEasy
  if (guessCount === 1) {
    guessesHardHigh.textContent = 'Too High: ';
    guessesHardLow.textContent = 'Too Low: ';
  }
 
  if (userGuess === randomNumber) {
    lastResultHard.textContent = 'Congratulations! You got it right!';
    lastResultHard.style.backgroundColor = 'limeGreen';
    lowOrHiHard.textContent = '';
    setGameOverHard();
  } else if (guessCount === 15) {
    lastResultHard.textContent = '*Sad Violin Noises*';
    setGameOverHard();
  } else { 
    lastResultHard.textContent = 'Wrong!';
    lastResultHard.style.backgroundColor = 'orangeRed';
    if(userGuess < randomNumber) {
      guessesHardLow.textContent += userGuess + ' ';
      lowOrHiHard.textContent = 'Last guess was too low!';
    } else if(userGuess > randomNumber) {
      guessesHardHigh.textContent += userGuess + ' ';
      lowOrHiHard.textContent = 'Last guess was too high!';
    }
  }
 
  guessCount++;
  guessFieldHard.value = '';
  guessFieldHard.focus();
}

//Tell the browser to run the checkGuess() function each time the user submits a guess.
guessSubmitHard.addEventListener('click', checkGuessHard);


function setGameOverHard() {
  guessFieldHard.disabled = true;
  guessSubmitHard.disabled = true;
  resetButtonHard = document.createElement('button');
  resetButtonHard.textContent = 'Start new game';
  gameResetHard.appendChild(resetButtonHard);
  resetButtonHard.addEventListener('click', resetGameHard);
}


function resetGameHard() {
  guessCount = 1;

  const resetParas = document.querySelectorAll('.gameHard p');
  for (let i = 0 ; i < resetParas.length ; i++) {
    resetParas[i].textContent = '';
  }

  resetButtonHard.parentNode.removeChild(resetButtonHard);

  guessFieldHard.disabled = false;
  guessSubmitHard.disabled = false;
  guessFieldHard.value = '';
  guessFieldHard.focus();

  lastResultHard.style.backgroundColor = 'white';

  randomNumber = Math.floor(Math.random() * 1000) + 1;
}
